import { GET_KNOTS, ADD_FAVORITE, REMOVE_FAVORITE, CATEGORY_KNOTS,CHOOSE_CATEGORY, CHOOSE_KNOT } from "../types"


export const getKnots = () =>{
    // return async dispatch =>{
    //     const response = await fetch('/getknots')
    //     const payload = await response.json()
    //     console.log(payload)

    //     await dispatch({
    //         type: GET_KNOTS,
    //         payload
    //     })
    // }
    // console.log('get')
    return{
        type: GET_KNOTS
    }
}

export const knotsByCategory = (payload) =>{
    return{
        type: CATEGORY_KNOTS,
        payload
    }
}

export const chooseCategory = (payload) =>{
    return {
        type: CHOOSE_CATEGORY,
        payload
    }
}

export const addFavorite = (payload) =>{
    return{
        type: ADD_FAVORITE,
        payload
    }
}

export const removeFavorite = (payload) =>{
    return{
        type: REMOVE_FAVORITE,
        payload
    }
}

export const chooseKnot = (payload) =>{
    return {
        type: CHOOSE_KNOT,
        payload
    }
}