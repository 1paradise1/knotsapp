import { SET_LANGUAGE } from "../types"


export const setLanguage = (payload) =>{
    return{
        type: SET_LANGUAGE,
        payload
    }
}