import { combineReducers } from 'redux';
import knotsReducer from './knotsReducer';
import languageReducer from './languageReducer';

export const rootReducer = combineReducers({
    knotsReducer,
    languageReducer
})