import { FETCHED_KNOTS, ADD_FAVORITE, REMOVE_FAVORITE, CATEGORY_KNOTS, CHOOSE_CATEGORY, CHOOSE_KNOT } from "../types"


const initialState = {
    knots: [],
    favKnots: [],
    categoryKnots:[],
    choosenCategory: '',
    choosenKnot: null
}

const knotsReducer = (state = initialState, action) =>{
    switch(action.type){
        case FETCHED_KNOTS:
            return{...state, knots:action.payload}
        case CATEGORY_KNOTS:
            return{...state, categoryKnots: action.payload}
        case CHOOSE_CATEGORY:
            return{...state, choosenCategory: action.payload}
        case ADD_FAVORITE:
            return{...state, favKnots: state.favKnots.concat([action.payload])}
        case REMOVE_FAVORITE:
            // let newFav = state.favKnots.slice()
            // newFav.map((fav,i)=>{
            //     if(fav.knotennummer === action.payload.knotennummer){
            //         newFav.splice(i,1)
            //     }
            // })
            // // console.log(state.favKnots)
            // return{...state, favKno ts:newFav}
            return{
                ...state,
                favKnots:[
                    ...state.favKnots.filter(knot =>  JSON.stringify(knot) !== JSON.stringify(action.payload) 
                    )
                ]
            }
        case CHOOSE_KNOT:
            return {...state, choosenKnot: action.payload}
        default: return state
    }
}

export default knotsReducer