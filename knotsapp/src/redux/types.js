export const GET_KNOTS = 'KNOTS/GET_KNOTS'
export const FETCHED_KNOTS = 'KNOTS/FETCHED_KNOTS'
export const CATEGORY_KNOTS = 'KNOTS/CATEGORY_KNOTS'
export const CHOOSE_CATEGORY = 'KNOTS/CHOOSE_CATEGORY'
export const ADD_FAVORITE = 'KNOTS/ADD_FAVORITE'
export const REMOVE_FAVORITE = 'KNOTS/REMOVE_FAVORITE'
export const CHOOSE_KNOT = 'KNOTS/CHOOSE_KNOT'

export const SET_LANGUAGE = 'LANG/SET_LANGUAGE'