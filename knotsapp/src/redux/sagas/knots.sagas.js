import {takeEvery, put, call} from 'redux-saga/effects'
import { GET_KNOTS, FETCHED_KNOTS } from '../types'


export function* sagaWatcher() {
    yield takeEvery(GET_KNOTS, sagaWorker)
}

function* sagaWorker() {
    const payload = yield call(fetchKnots)
    yield put({type: FETCHED_KNOTS, payload})
}

async function fetchKnots(){
    const response = await fetch('/getknots')
    return await response.json()
}