import React from 'react'
import '../assets/css/knots.scss'
import knotsList from '../assets/knots'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addFavorite, removeFavorite, chooseKnot } from '../redux/actions/knotsActions';
import { useEffect } from 'react';
import Knot from './knot.component';


const Knots = () => {

    const dispatch = useDispatch()
    const favKnots = useSelector(state => state.knotsReducer.favKnots)
    const knots = useSelector(state => state.knotsReducer.categoryKnots)
    const category = useSelector(state => state.knotsReducer.choosenCategory)

    const language = useSelector(state => state.languageReducer.language)


    const [isChoosen, setChoose] = useState(false)
    const [inputvalue, setValue] = useState(null)

    const showknots = (knot) => {
        let link = ''
        knotsList.map((knotItem, i) => {
            if (knotItem.knotennummer === +knot.knotennummer) {
                link = knotItem.imagePreview
            }
        })
        if (link === '') {
            link = require(`../assets/listimage/${knot.knotenbild2d}`)
        }
        return link
    }

    const checkFavorite = (knot) => {
        let isFav = false
        favKnots.map((fknot, i) => {
            if (knot.knotennummer === fknot.knotennummer) {
                isFav = true
            }
        })
        return isFav
    }

    const addRemFavorite = (knot) => {
        checkFavorite(knot) ? dispatch(removeFavorite(knot)) : dispatch(addFavorite(knot))
    }
    const findKnot = (search, knots) => {
        let result = []
        knots.map((knot) => {
            if (knot[`knotenname_${language}`].toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                result.push(knot)
            }
        })
        return result
    }

    const checkCategory = (search) => {

        if ((search && category === 'favorite')) {
            return findKnot(search, favKnots)
        } else
            if (search) {
                return findKnot(search, knots)
            } else
                if (category === 'favorite') {
                    return favKnots
                } else {
                    return knots
                }

    }
    const chooseknot = (knot) => {
        dispatch(chooseKnot(knot))
        setChoose(true)
    }

    const handleChange = (e) => {
        setValue(e.target.value)
    }

    return (
        <div className="knots-container">
            <div className="search-knots">
                <input onChange={handleChange}  type="text" />
            </div>
            <div className="knots-container-item">
                <div className="knots">
                    {checkCategory(inputvalue && inputvalue).map((knot, i) => {
                        const link = showknots(knot)
                        let knotName = knot[`knotenname_${language}`].split('_')
                        const isFavorite = checkFavorite(knot)
                        return (
                            <div key={i} className="knots-item">
                                <div className="knots-item-imagecont">
                                    <img className="knots-item-image" src={link} alt="" />
                                    <FontAwesomeIcon onClick={() => addRemFavorite(knot)} className={isFavorite ? 'knot-item-icon-choosen' : 'knot-item-icon'} icon={faHeart} />
                                </div>
                                <p onClick={() => chooseknot(knot)} className="knots-item-text">{knotName[0]}</p>
                            </div>
                        )
                    })}
                </div>
                {isChoosen &&
                    <div className="knots-knot">
                        <Knot />
                    </div>
                }
            </div>

        </div>

    )
}

export default Knots