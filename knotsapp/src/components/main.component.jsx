import React from 'react'
import categories from '../assets/categories'
import { useState } from 'react'
import Knots from './knots.component'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getKnots, knotsByCategory, chooseCategory } from '../redux/actions/knotsActions'

import languages from '../assets/languages.json'
import '../assets/css/main.scss'
import { setLanguage } from '../redux/actions/languageActions'
const Main = () => {
    const dispatch = useDispatch()
    const fetchedKnots = useSelector(state => state.knotsReducer.knots.knots)
    const favKnots = useSelector(state => state.knotsReducer.favKnots)
    const [isSplited, setSplit] = useState(false)
    const language = useSelector(state => state.languageReducer.language)

    useEffect(() => {
        dispatch(getKnots())
    }, [dispatch])

    const findNames = () => {
        const result = []
        categories.map((category) => {
            if (category.type_eng === 'Category') {
                result[0] = category[`type_${language}`]
            }
            if (category.type_eng === 'Type') {
                result[1] = category[`type_${language}`]
            }
        })
        return result
    }

    const findedNames = findNames()


    const splitByCategory = () => {
        fetchedKnots.map((knot) => {
            knot.knoten_typ = knot.knoten_typ.split('_')
        })
        setSplit(true)
    }

    const choosenCat = (category) => {
        !isSplited && splitByCategory()
        let result = []
        fetchedKnots.map((knot) => {
            for (let type of knot.knoten_typ) {
                if (type === category.code) {
                    result.push(knot)
                }
            }

        })
        if (category.code === 'favorite') {
            result = favKnots.slice()
        }
        if (result.length === 0 && category.code !== 'favorite') {
            dispatch(knotsByCategory(fetchedKnots))
            dispatch(chooseCategory('all'))
        } else {
            dispatch(knotsByCategory(result))
            dispatch(chooseCategory(category.code))
        }
    }

    const handleChangeLang = (e) => {
        dispatch(setLanguage(e.target.value))
    }

    return (
        <div className="main">
            <div className="main-language">
                <select value={language} onChange={handleChangeLang}>
                    {languages.map((lang, i) => {
                        if (lang.code === 'ru') {
                            return (
                                <option key={i} value={lang.code}>{lang.name}</option>
                            )
                        } else {
                            return (
                                <option key={i} value={lang.code}>{lang.name}</option>
                            )
                        }

                    })}
                </select>
            </div>
            <div className="main-items">
                <div className="main-category">
                    <div className="main-category-item main-category-name">
                        <p>{findedNames[0]}</p>
                    </div>
                    {categories.map((category, i) => {
                        if (category.type_eng === 'Category' || category.type_eng === 'Collection') {
                            return (
                                <div className="main-category-item" key={i} onClick={() => choosenCat(category)}>
                                    <img className="main-category-image" src={category.image} alt="" />
                                    {category[`name_${language}`]}
                                </div>
                            )
                        }

                    })}
                    <div className="main-category-item main-category-name">
                        <p>{findedNames[1]}</p>
                    </div>
                    {categories.map((category, i) => {
                        if (category.type_eng === 'Type') {
                            return (
                                <div className="main-category-item" key={i} onClick={() => choosenCat(category)}>
                                    <img className="main-category-image" src={category.image} alt="" />
                                    {category[`name_${language}`]}
                                </div>
                            )
                        }

                    })}
                </div>
                <div className="main-knots">
                    <Knots />
                </div>
            </div>

        </div>
    )
}


export default Main