import React, { useState, useEffect } from 'react'
// import SpriteAnimator  from 'react-sequence-animator'
import { SpriteAnimator, useSprite } from 'react-sprite-animator'
import RangeSlider from 'react-bootstrap-range-slider';
import '../assets/css/animation.scss'
const Animate = ({ knot }) => {

    useEffect(() => {
        console.log('rerender')
    })


    const [slideValue, setSlide] = useState(6)
    const [frame, set3d] = useState('2d')
    const [isStarted, setStart] = useState(true)


    const link = require(`../assets/images/${knot[`knotenbild${frame}`]}`)

    const styles = useSprite({
        sprite:link,
        width:+knot.knoten_frameweite,
        height:+knot.knoten_framehoehe,
        frameCount:+knot[`knoten_frame_${frame}`],
        wrapAfter:+knot[`knoten_count_x_${frame}`],
        fps:slideValue,
        shouldAnimate:isStarted,
        startFrame: 0
    })
    // const handleChanhe = e =>{
    //     setSlide(e.target.value)
    // }

    return (
        <div className="animation">
            {/* <RangeSlider 
                value={slideValue}
                onChange={handleChanhe}
                min={0}
                max={13}
            /> */}

            <div className="animation-knot" style={styles}></div>
            <button onClick={() => setStart(true)}>Start</button>
            <button onClick={() => setStart(false)}>Stop</button>

            <button onClick={() => set3d('2d')}>2D</button>
            <button onClick={() => set3d('360')}>3D</button>
        </div>
    )
}

export default Animate