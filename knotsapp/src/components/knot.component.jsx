import React from 'react'
import { useSelector } from 'react-redux'
import categories from '../assets/categories'
import Animate from '../components/animation.component'
import localisation from '../assets/staticLocalisation.json'

import '../assets/css/knot.scss'

const Knot = () => {
    const knot = useSelector(state => state.knotsReducer.choosenKnot)
    const language = useSelector(state => state.languageReducer.language)
    const cat = categories[3][`type_${language}`]
    const names = knot[`knotenname_${language}`].split('_')

    const getCategories = (typ) => {
        let result = []
        categories.map(category => {
            for (const t of typ) {
                if (t === category.code) {
                    result.push(category[`name_${language}`])
                }
            }
        })
        return result
    }

    const receivedCategories = getCategories(knot.knoten_typ)


    return (
        <div className="knot">
            <div className="knot-item">
                <Animate knot={knot} />
                <p className="knot-description"><strong>{localisation.description[`name_${language}`]}:</strong> {knot[`knotenbeschreibung_${language}`]}</p>
                <p className="knot-abok"><strong>АВОК:</strong> {knot.knoten_abok}</p>
                <p className="knot-names"><strong>{localisation.alias[`name_${language}`]}:</strong>
                    {names.map((name, i) => {
                        return (
                            <li key={i}>{name}</li>
                        )
                    })}
                </p>
                <p className="knot-percent"><strong>{localisation.strength[`name_${language}`]}:</strong> {knot.knotenfestigkeit}</p>
                <p className="categories"><strong>{cat}:</strong>
                    {
                        receivedCategories.map((category, i) => {
                            return (
                                <li key={i}>{category}</li>
                            )
                        })
                    }
                </p>
            </div>

        </div>
    )
}

export default Knot