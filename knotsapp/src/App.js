import React from 'react';
import logo from './logo.svg';
import './App.css';
import Main from './components/main.component';
// import Header from './components/header';

function App() {
  return (
    <div className="app">
     
      <Main />
    </div>
  );
}

export default App;
