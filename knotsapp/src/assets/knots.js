export default [
  {
    knotennummer: 1,
    imagePreview: require('./listimage/draw_achterknoten_2d_1.png'),
    image2d: require('./images/draw_achterknoten_2d_1.png'),
    image360: require('./images/draw_achterknoten_360_1.png')
  },
  {
    knotennummer: 2,
    imagePreview: require('./listimage/draw_achterknotenschlaufe_2d_1.png'),
    image2d: require('./images/draw_achterknotenschlaufe_2d_1.png'),
    image360: require('./images/draw_achterknotenschlaufe_360_1.png')
  },
  {
    knotennummer: 3,
    imagePreview: require('./listimage/draw_affenfaustknoten_2d_1.png'),
    image2d: require('./images/draw_affenfaustknoten_2d_1.png'),
    image360: require('./images/draw_affenfaustknoten_360_1.png')
  },
  {
    knotennummer: 4,
    imagePreview: require('./listimage/draw_albrightknoten_2d_1.png'),
    image2d: require('./images/draw_albrightknoten_2d_1.png'),
    image360: require('./images/draw_albrightknoten_360_1.png')
  },
  {
    knotennummer: 5,
    imagePreview: require('./listimage/draw_altweiber_2d_1.png'),
    image2d: require('./images/draw_altweiber_2d_1.png'),
    image360: require('./images/draw_altweiber_360_1.png')
  },
  {
    knotennummer: 6,
    imagePreview: require('./listimage/draw_anbindeknoten_2d_1.png'),
    image2d: require('./images/draw_anbindeknoten_2d_1.png'),
    image360: require('./images/draw_anbindeknoten_360_1.png')
  },
  {
    knotennummer: 7,
    imagePreview: require('./listimage/draw_anglerschlaufe_2d_1.png'),
    image2d: require('./images/draw_anglerschlaufe_2d_1.png'),
    image360: require('./images/draw_anglerschlaufe_360_1.png')
  },
  {
    knotennummer: 8,
    imagePreview: require('./listimage/draw_arborknoten_2d_1.png'),
    image2d: require('./images/draw_arborknoten_2d_1.png'),
    image360: require('./images/draw_arborknoten_360_1.png')
  },
  {
    knotennummer: 9,
    imagePreview: require('./listimage/draw_ashleystek_2d_1.png'),
    image2d: require('./images/draw_ashleystek_2d_1.png'),
    image360: require('./images/draw_ashleystek_360_1.png')
  },
  {
    knotennummer: 10,
    imagePreview: require('./listimage/draw_ashleystopperkn_2d_1.png'),
    image2d: require('./images/draw_ashleystopperkn_2d_1.png'),
    image360: require('./images/draw_ashleystopperkn_360_1.png')
  },
  {
    knotennummer: 11,
    imagePreview: require('./listimage/draw_bandschlingenkn_2d_1.png'),
    image2d: require('./images/draw_bandschlingenkn_2d_1.png'),
    image360: require('./images/draw_bandschlingenkn_360_1.png')
  },
  {
    knotennummer: 12,
    imagePreview: require('./listimage/draw_blakeknoten_2d_1.png'),
    image2d: require('./images/draw_blakeknoten_2d_1.png'),
    image360: require('./images/draw_blakeknoten_360_1.png')
  },
  {
    knotennummer: 13,
    imagePreview: require('./listimage/draw_blutknoten_2d_1.png'),
    image2d: require('./images/draw_blutknoten_2d_1.png'),
    image360: require('./images/draw_blutknoten_360_1.png')
  },
  {
    knotennummer: 14,
    imagePreview: require('./listimage/draw_bootsmannknoten_2d_1.png'),
    image2d: require('./images/draw_bootsmannknoten_2d_1.png'),
    image360: require('./images/draw_bootsmannknoten_360_1.png')
  },
  {
    knotennummer: 15,
    imagePreview: require('./listimage/draw_chirugenknoten_2d_1.png'),
    image2d: require('./images/draw_chirugenknoten_2d_1.png'),
    image360: require('./images/draw_chirugenknoten_360_1.png')
  },
  {
    knotennummer: 16,
    imagePreview: require('./listimage/draw_chirugenfischknoten_2d_1.png'),
    image2d: require('./images/draw_chirugenfischknoten_2d_1.png'),
    image360: require('./images/draw_chirugenfischknoten_360_1.png')
  },
  {
    knotennummer: 17,
    imagePreview: require('./listimage/draw_clinchknoten_2d_1.png'),
    image2d: require('./images/draw_clinchknoten_2d_1.png'),
    image360: require('./images/draw_clinchknoten_360_1.png')
  },
  {
    knotennummer: 18,
    imagePreview: require('./listimage/draw_davyknoten_2d_1.png'),
    image2d: require('./images/draw_davyknoten_2d_1.png'),
    image360: require('./images/draw_davyknoten_360_1.png')
  },
  {
    knotennummer: 19,
    imagePreview: require('./listimage/draw_diagomalbund_2d_1.png'),
    image2d: require('./images/draw_diagomalbund_2d_1.png'),
    image360: require('./images/draw_diagomalbund_360_1.png')
  },
  {
    knotennummer: 20,
    imagePreview: require('./listimage/draw_diamantknoten_2d_1.png'),
    image2d: require('./images/draw_diamantknoten_2d_1.png'),
    image360: require('./images/draw_diamantknoten_360_1.png')
  },
  {
    knotennummer: 21,
    imagePreview: require('./listimage/draw_diebesknoten_2d_1.png'),
    image2d: require('./images/draw_diebesknoten_2d_1.png'),
    image360: require('./images/draw_diebesknoten_360_1.png')
  },
  {
    knotennummer: 22,
    imagePreview: require('./listimage/draw_distelknoten_2d_1.png'),
    image2d: require('./images/draw_distelknoten_2d_1.png'),
    image360: require('./images/draw_distelknoten_360_1.png')
  },
  {
    knotennummer: 23,
    imagePreview: require('./listimage/draw_hasenohren_2d_1.png'),
    image2d: require('./images/draw_hasenohren_2d_1.png'),
    image360: require('./images/draw_hasenohren_360_1.png')
  },
  {
    knotennummer: 24,
    imagePreview: require('./listimage/draw_doppeltachtknoten_2d_1.png'),
    image2d: require('./images/draw_doppeltachtknoten_2d_1.png'),
    image360: require('./images/draw_doppeltachtknoten_360_1.png')
  },
  {
    knotennummer: 25,
    imagePreview: require('./listimage/draw_doppeltkonstriktorknoten_2d_1.png'),
    image2d: require('./images/draw_doppeltkonstriktorknoten_2d_1.png'),
    image360: require('./images/draw_doppeltkonstriktorknoten_360_1.png')
  },
  {
    knotennummer: 26,
    imagePreview: require('./listimage/draw_doppeltmuenz_2d_1.png'),
    image2d: require('./images/draw_doppeltmuenz_2d_1.png'),
    image360: require('./images/draw_doppeltmuenz_360_1.png')
  },
  {
    knotennummer: 27,
    imagePreview: require('./listimage/draw_doppeltpalstek_2d_1.png'),
    image2d: require('./images/draw_doppeltpalstek_2d_1.png'),
    image360: require('./images/draw_doppeltpalstek_360_1.png')
  },
  {
    knotennummer: 28,
    imagePreview: require('./listimage/draw_doppeltschlaufenknoten_2d_1.png'),
    image2d: require('./images/draw_doppeltschlaufenknoten_2d_1.png'),
    image360: require('./images/draw_doppeltschlaufenknoten_360_1.png')
  },
  {
    knotennummer: 29,
    imagePreview: require('./listimage/draw_doppelterschotstek_2d_1.png'),
    image2d: require('./images/draw_doppelterschotstek_2d_1.png'),
    image360: require('./images/draw_doppelterschotstek_360_1.png')
  },
  {
    knotennummer: 30,
    imagePreview: require('./listimage/draw_doppelterspierenstich_2d_1.png'),
    image2d: require('./images/draw_doppelterspierenstich_2d_1.png'),
    image360: require('./images/draw_doppelterspierenstich_360_1.png')
  },
  {
    knotennummer: 31,
    imagePreview: require('./listimage/draw_doppelterueberhand_2d_1.png'),
    image2d: require('./images/draw_doppelterueberhand_2d_1.png'),
    image360: require('./images/draw_doppelterueberhand_360_1.png')
  },
  {
    knotennummer: 32,
    imagePreview: require('./listimage/draw_dreibeinbund_2d_1.png'),
    image2d: require('./images/draw_dreibeinbund_2d_1.png'),
    image360: require('./images/draw_dreibeinbund_360_1.png')
  },
  {
    knotennummer: 33,
    imagePreview: require('./listimage/draw_echtliebesknoten_2d_1.png'),
    image2d: require('./images/draw_echtliebesknoten_2d_1.png'),
    image360: require('./images/draw_echtliebesknoten_360_1.png')
  },
  {
    knotennummer: 34,
    imagePreview: require('./listimage/draw_einfachertakling_2d_1.png'),
    image2d: require('./images/draw_einfachertakling_2d_1.png'),
    image360: require('./images/draw_einfachertakling_360_1.png')
  },
  {
    knotennummer: 35,
    imagePreview: require('./listimage/draw_einfachseilgeflecht_2d_1.png'),
    image2d: require('./images/draw_einfachseilgeflecht_2d_1.png'),
    image360: require('./images/draw_einfachseilgeflecht_360_1.png')
  },
  {
    knotennummer: 36,
    imagePreview: require('./listimage/draw_eiszapfenstek_2d_1.png'),
    image2d: require('./images/draw_eiszapfenstek_2d_1.png'),
    image360: require('./images/draw_eiszapfenstek_360_1.png')
  },
  {
    knotennummer: 37,
    imagePreview: require('./listimage/draw_elektrikerknoten_2d_1.png'),
    image2d: require('./images/draw_elektrikerknoten_2d_1.png'),
    image360: require('./images/draw_elektrikerknoten_360_1.png')
  },
  {
    knotennummer: 38,
    imagePreview: require('./listimage/draw_englaenderschlaufe_2d_1.png'),
    image2d: require('./images/draw_englaenderschlaufe_2d_1.png'),
    image360: require('./images/draw_englaenderschlaufe_360_1.png')
  },
  {
    knotennummer: 39,
    imagePreview: require('./listimage/draw_fassschlag_2d_1.png'),
    image2d: require('./images/draw_fassschlag_2d_1.png'),
    image360: require('./images/draw_fassschlag_360_1.png')
  },
  {
    knotennummer: 40,
    imagePreview: require('./listimage/draw_fassschlag2_2d_1.png'),
    image2d: require('./images/draw_fassschlag2_2d_1.png'),
    image360: require('./images/draw_fassschlag2_360_1.png')
  },
  {
    knotennummer: 41,
    imagePreview: require('./listimage/draw_fesselknoten_2d_1.png'),
    image2d: require('./images/draw_fesselknoten_2d_1.png'),
    image360: require('./images/draw_fesselknoten_360_1.png')
  },
  {
    knotennummer: 42,
    imagePreview: require('./listimage/draw_flaemischschleife_2d_1.png'),
    image2d: require('./images/draw_flaemischschleife_2d_1.png'),
    image360: require('./images/draw_flaemischschleife_360_1.png')
  },
  {
    knotennummer: 43,
    imagePreview: require('./listimage/draw_flaemischachtnoten_2d_1.png'),
    image2d: require('./images/draw_flaemischachtnoten_2d_1.png'),
    image360: require('./images/draw_flaemischachtnoten_360_1.png')
  },
  {
    knotennummer: 44,
    imagePreview: require('./listimage/draw_jansiktnoten_2d_1.png'),
    image2d: require('./images/draw_jansiktnoten_2d_1.png'),
    image360: require('./images/draw_jansiktnoten_360_1.png')
  },
  {
    knotennummer: 45,
    imagePreview: require('./listimage/draw_fuehrerknoten_2d_1.png'),
    image2d: require('./images/draw_fuehrerknoten_2d_1.png'),
    image360: require('./images/draw_fuehrerknoten_360_1.png')
  },
  {
    knotennummer: 46,
    imagePreview: require('./listimage/draw_geruestknoten_2d_1.png'),
    image2d: require('./images/draw_geruestknoten_2d_1.png'),
    image360: require('./images/draw_geruestknoten_360_1.png')
  },
  {
    knotennummer: 47,
    imagePreview: require('./listimage/draw_geslippterschotstek_2d_1.png'),
    image2d: require('./images/draw_geslippterschotstek_2d_1.png'),
    image360: require('./images/draw_geslippterschotstek_360_1.png')
  },
  {
    knotennummer: 48,
    imagePreview: require('./listimage/draw_gesteckterachterknoten_2d_1.png'),
    image2d: require('./images/draw_gesteckterachterknoten_2d_1.png'),
    image360: require('./images/draw_gesteckterachterknoten_360_1.png')
  },
  {
    knotennummer: 49,
    imagePreview: require('./listimage/draw_gordingstek_2d_1.png'),
    image2d: require('./images/draw_gordingstek_2d_1.png'),
    image360: require('./images/draw_gordingstek_360_1.png')
  },
  {
    knotennummer: 50,
    imagePreview: require('./listimage/draw_grinnerdoppelter_2d_1.png'),
    image2d: require('./images/draw_grinnerdoppelter_2d_1.png'),
    image360: require('./images/draw_grinnerdoppelter_360_1.png')
  },
  {
    knotennummer: 51,
    imagePreview: require('./listimage/draw_grinnerhaken_2d_1.png'),
    image2d: require('./images/draw_grinnerhaken_2d_1.png'),
    image360: require('./images/draw_grinnerhaken_360_1.png')
  },
  {
    knotennummer: 52,
    imagePreview: require('./listimage/draw_grinnerknoten_2d_1.png'),
    image2d: require('./images/draw_grinnerknoten_2d_1.png'),
    image360: require('./images/draw_grinnerknoten_360_1.png')
  },
  {
    knotennummer: 53,
    imagePreview: require('./listimage/draw_halbmastwurf_2d_1.png'),
    image2d: require('./images/draw_halbmastwurf_2d_1.png'),
    image360: require('./images/draw_halbmastwurf_360_1.png')
  },
  {
    knotennummer: 54,
    imagePreview: require('./listimage/draw_halbschlag_2d_1.png'),
    image2d: require('./images/draw_halbschlag_2d_1.png'),
    image360: require('./images/draw_halbschlag_360_1.png')
  },
  {
    knotennummer: 55,
    imagePreview: require('./listimage/draw_heckknoten_2d_1.png'),
    image2d: require('./images/draw_heckknoten_2d_1.png'),
    image360: require('./images/draw_heckknoten_360_1.png')
  },
  {
    knotennummer: 56,
    imagePreview: require('./listimage/draw_hondaknoten_2d_1.png'),
    image2d: require('./images/draw_hondaknoten_2d_1.png'),
    image360: require('./images/draw_hondaknoten_360_1.png')
  },
  {
    knotennummer: 57,
    imagePreview: require('./listimage/draw_konstriktorknoten_2d_1.png'),
    image2d: require('./images/draw_konstriktorknoten_2d_1.png'),
    image360: require('./images/draw_konstriktorknoten_360_1.png')
  },
  {
    knotennummer: 58,
    imagePreview: require('./listimage/draw_kreuzbund_2d_1.png'),
    image2d: require('./images/draw_kreuzbund_2d_1.png'),
    image360: require('./images/draw_kreuzbund_360_1.png')
  },
  {
    knotennummer: 59,
    imagePreview: require('./listimage/draw_kreuzklemmknoten_2d_1.png'),
    image2d: require('./images/draw_kreuzklemmknoten_2d_1.png'),
    image360: require('./images/draw_kreuzklemmknoten_360_1.png')
  },
  {
    knotennummer: 60,
    imagePreview: require('./listimage/draw_kreuzknoten_2d_1.png'),
    image2d: require('./images/draw_kreuzknoten_2d_1.png'),
    image360: require('./images/draw_kreuzknoten_360_1.png')
  },
  {
    knotennummer: 61,
    imagePreview: require('./listimage/draw_fesselmitschlag_2d_1.png'),
    image2d: require('./images/draw_fesselmitschlag_2d_1.png'),
    image360: require('./images/draw_fesselmitschlag_360_1.png')
  },
  {
    knotennummer: 62,
    imagePreview: require('./listimage/draw_kuhstek_2d_1.png'),
    image2d: require('./images/draw_kuhstek_2d_1.png'),
    image360: require('./images/draw_kuhstek_360_1.png')
  },
  {
    knotennummer: 63,
    imagePreview: require('./listimage/draw_kurzetrompete_2d_1.png'),
    image2d: require('./images/draw_kurzetrompete_2d_1.png'),
    image360: require('./images/draw_kurzetrompete_360_1.png')
  },
  {
    knotennummer: 64,
    imagePreview: require('./listimage/draw_laufenderpalstek_2d_1.png'),
    image2d: require('./images/draw_laufenderpalstek_2d_1.png'),
    image360: require('./images/draw_laufenderpalstek_360_1.png')
  },
  {
    knotennummer: 65,
    imagePreview: require('./listimage/draw_lerchenkopf_2d_1.png'),
    image2d: require('./images/draw_lerchenkopf_2d_1.png'),
    image360: require('./images/draw_lerchenkopf_360_1.png')
  },
  {
    knotennummer: 66,
    imagePreview: require('./listimage/draw_laengsbund_2d_1.png'),
    image2d: require('./images/draw_laengsbund_2d_1.png'),
    image360: require('./images/draw_laengsbund_360_1.png')
  },
  {
    knotennummer: 67,
    imagePreview: require('./listimage/draw_marlspiekerschlag_2d_1.png'),
    image2d: require('./images/draw_marlspiekerschlag_2d_1.png'),
    image360: require('./images/draw_marlspiekerschlag_360_1.png')
  },
  {
    knotennummer: 68,
    imagePreview: require('./listimage/draw_nagelknoten_2d_1.png'),
    image2d: require('./images/draw_nagelknoten_2d_1.png'),
    image360: require('./images/draw_nagelknoten_360_1.png')
  },
  {
    knotennummer: 69,
    imagePreview: require('./listimage/draw_notmastknoten_2d_1.png'),
    image2d: require('./images/draw_notmastknoten_2d_1.png'),
    image360: require('./images/draw_notmastknoten_360_1.png')
  },
  {
    knotennummer: 70,
    imagePreview: require('./listimage/draw_packerknoten_2d_1.png'),
    image2d: require('./images/draw_packerknoten_2d_1.png'),
    image360: require('./images/draw_packerknoten_360_1.png')
  },
  {
    knotennummer: 71,
    imagePreview: require('./listimage/draw_palomarknoten_2d_1.png'),
    image2d: require('./images/draw_palomarknoten_2d_1.png'),
    image360: require('./images/draw_palomarknoten_360_1.png')
  },
  {
    knotennummer: 72,
    imagePreview: require('./listimage/draw_palstekaussen_2d_1.png'),
    image2d: require('./images/draw_palstekaussen_2d_1.png'),
    image360: require('./images/draw_palstekaussen_360_1.png')
  },
  {
    knotennummer: 73,
    imagePreview: require('./listimage/draw_palstekinnen_2d_1.png'),
    image2d: require('./images/draw_palstekinnen_2d_1.png'),
    image360: require('./images/draw_palstekinnen_360_1.png')
  },
  {
    knotennummer: 74,
    imagePreview: require('./listimage/draw_rundtoernpalstek_2d_1.png'),
    image2d: require('./images/draw_wasserpalstek_2d_1.png'),
    image360: require('./images/draw_wasserpalstek_360_1.png')
  },
  {
    knotennummer: 75,
    imagePreview: require('./listimage/draw_parallelbund_2d_1.png'),
    image2d: require('./images/draw_parallelbund_2d_1.png'),
    image360: require('./images/draw_parallelbund_360_1.png')
  },
  {
    knotennummer: 76,
    imagePreview: require('./listimage/draw_pfahlknoten_2d_1.png'),
    image2d: require('./images/draw_pfahlknoten_2d_1.png'),
    image360: require('./images/draw_pfahlknoten_360_1.png')
  },
  {
    knotennummer: 77,
    imagePreview: require('./listimage/draw_prusikknoten_2d_1.png'),
    image2d: require('./images/draw_prusikknoten_2d_1.png'),
    image360: require('./images/draw_prusikknoten_360_1.png')
  },
  {
    knotennummer: 78,
    imagePreview: require('./listimage/draw_rapalaknoten_2d_1.png'),
    image2d: require('./images/draw_rapalaknoten_2d_1.png'),
    image360: require('./images/draw_rapalaknoten_360_1.png')
  },
  {
    knotennummer: 79,
    imagePreview: require('./listimage/draw_roringstek_2d_1.png'),
    image2d: require('./images/draw_roringstek_2d_1.png'),
    image360: require('./images/draw_roringstek_360_1.png')
  },
  {
    knotennummer: 80,
    imagePreview: require('./listimage/draw_rundtoernpalstek_2d_1.png'),
    image2d: require('./images/draw_rundtoernpalstek_2d_1.png'),
    image360: require('./images/draw_rundtoernpalstek_360_1.png')
  },
  {
    knotennummer: 81,
    imagePreview: require('./listimage/draw_rundtoernmithalbschlag_2d_1.png'),
    image2d: require('./images/draw_rundtoernmithalbschlag_2d_1.png'),
    image360: require('./images/draw_rundtoernmithalbschlag_360_1.png')
  },
  {
    knotennummer: 82,
    imagePreview: require('./listimage/draw_raeuberknoten_2d_1.png'),
    image2d: require('./images/draw_raeuberknoten_2d_1.png'),
    image360: require('./images/draw_raeuberknoten_360_1.png')
  },
  {
    knotennummer: 83,
    imagePreview: require('./listimage/draw_sackstich_2d_1.png'),
    image2d: require('./images/draw_sackstich_2d_1.png'),
    image360: require('./images/draw_sackstich_360_1.png')
  },
  {
    knotennummer: 84,
    imagePreview: require('./listimage/draw_sandiegojam_2d_1.png'),
    image2d: require('./images/draw_sandiegojam_2d_1.png'),
    image360: require('./images/draw_sandiegojam_360_1.png')
  },
  {
    knotennummer: 85,
    imagePreview: require('./listimage/draw_schauermann_2d_1.png'),
    image2d: require('./images/draw_schauermann_2d_1.png'),
    image360: require('./images/draw_schauermann_360_1.png')
  },
  {
    knotennummer: 86,
    imagePreview: require('./listimage/draw_schleife_2d_1.png'),
    image2d: require('./images/draw_schleife_2d_1.png'),
    image360: require('./images/draw_schleife_360_1.png')
  },
  {
    knotennummer: 87,
    imagePreview: require('./listimage/draw_schmetterling_2d_1.png'),
    image2d: require('./images/draw_schmetterling_2d_1.png'),
    image360: require('./images/draw_schmetterling_360_1.png')
  },
  {
    knotennummer: 88,
    imagePreview: require('./listimage/draw_schotstek_2d_1.png'),
    image2d: require('./images/draw_schotstek_2d_1.png'),
    image360: require('./images/draw_schotstek_360_1.png')
  },
  {
    knotennummer: 89,
    imagePreview: require('./listimage/draw_schwabisch_2d_1.png'),
    image2d: require('./images/draw_schwabisch_2d_1.png'),
    image360: require('./images/draw_schwabisch_360_1.png')
  },
  {
    knotennummer: 90,
    imagePreview: require('./listimage/draw_seemannskreuz_2d_1.png'),
    image2d: require('./images/draw_seemannskreuz_2d_1.png'),
    image360: require('./images/draw_seemannskreuz_360_1.png')
  },
  {
    knotennummer: 91,
    imagePreview: require('./listimage/draw_sibirischer_2d_1.png'),
    image2d: require('./images/draw_sibirischer_2d_1.png'),
    image360: require('./images/draw_sibirischer_360_1.png')
  },
  {
    knotennummer: 92,
    imagePreview: require('./listimage/draw_slipstek_2d_1.png'),
    image2d: require('./images/draw_slipstek_2d_1.png'),
    image360: require('./images/draw_slipstek_360_1.png')
  },
  {
    knotennummer: 93,
    imagePreview: require('./listimage/draw_spierenstich_2d_1.png'),
    image2d: require('./images/draw_spierenstich_2d_1.png'),
    image360: require('./images/draw_spierenstich_360_1.png')
  },
  {
    knotennummer: 94,
    imagePreview: require('./listimage/draw_springerschlaufe_2d_1.png'),
    image2d: require('./images/draw_springerschlaufe_2d_1.png'),
    image360: require('./images/draw_springerschlaufe_360_1.png')
  },
  {
    knotennummer: 95,
    imagePreview: require('./listimage/draw_stopperstek_2d_1.png'),
    image2d: require('./images/draw_stopperstek_2d_1.png'),
    image360: require('./images/draw_stopperstek_360_1.png')
  },
  {
    knotennummer: 96,
    imagePreview: require('./listimage/draw_topsegelsteg_2d_1.png'),
    image2d: require('./images/draw_topsegelsteg_2d_1.png'),
    image360: require('./images/draw_topsegelsteg_360_1.png')
  },
  {
    knotennummer: 97,
    imagePreview: require('./listimage/draw_trileneknoten_2d_1.png'),
    image2d: require('./images/draw_trileneknoten_2d_1.png'),
    image360: require('./images/draw_trileneknoten_360_1.png')
  },
  {
    knotennummer: 98,
    imagePreview: require('./listimage/draw_mastwurfgelegt_2d_1.png'),
    image2d: require('./images/draw_mastwurfgelegt_2d_1.png'),
    image360: require('./images/draw_mastwurfgelegt_360_1.png')
  },
  {
    knotennummer: 99,
    imagePreview: require('./listimage/draw_mastwurfgebunden_2d_1.png'),
    image2d: require('./images/draw_mastwurfgebunden_2d_1.png'),
    image360: require('./images/draw_mastwurfgebunden_360_1.png')
  },
  {
    knotennummer: 100,
    imagePreview: require('./listimage/draw_trompetenknoten_2d_1.png'),
    image2d: require('./images/draw_trompetenknoten_2d_1.png'),
    image360: require('./images/draw_trompetenknoten_360_1.png')
  },
  {
    knotennummer: 101,
    imagePreview: require('./listimage/draw_trossenstek_2d_1.png'),
    image2d: require('./images/draw_trossenstek_2d_1.png'),
    image360: require('./images/draw_trossenstek_360_1.png')
  },
  {
    knotennummer: 102,
    imagePreview: require('./listimage/draw_wurfleinenknoten_2d_1.png'),
    image2d: require('./images/draw_wurfleinenknoten_2d_1.png'),
    image360: require('./images/draw_wurfleinenknoten_360_1.png')
  },
  {
    knotennummer: 103,
    imagePreview: require('./listimage/draw_yosemite_2d_1.png'),
    image2d: require('./images/draw_yosemite_2d_1.png'),
    image360: require('./images/draw_yosemite_360_1.png')
  },
  {
    knotennummer: 104,
    imagePreview: require('./listimage/draw_yucatan_2d_1.png'),
    image2d: require('./images/draw_yucatan_2d_1.png'),
    image360: require('./images/draw_yucatan_360_1.png')
  },
  {
    knotennummer: 105,
    imagePreview: require('./listimage/draw_zeppelinstek_2d_1.png'),
    image2d: require('./images/draw_zeppelinstek_2d_1.png'),
    image360: require('./images/draw_zeppelinstek_360_1.png')
  },
  {
    knotennummer: 106,
    imagePreview: require('./listimage/draw_zimmermann_2d_1.png'),
    image2d: require('./images/draw_zimmermann_2d_1.png'),
    image360: require('./images/draw_zimmermann_360_1.png')
  },
  {
    knotennummer: 107,
    imagePreview: require('./listimage/draw_zweihalbschlaege_2d_1.png'),
    image2d: require('./images/draw_zweihalbschlaege_2d_1.png'),
    image360: require('./images/draw_zweihalbschlaege_360_1.png')
  },
  {
    knotennummer: 108,
    imagePreview: require('./listimage/draw_ueberhandknoten_2d_1.png'),
    image2d: require('./images/draw_ueberhandknoten_2d_1.png'),
    image360: require('./images/draw_ueberhandknoten_360_1.png')
  },
  {
    knotennummer: 109,
    imagePreview: require('./listimage/draw_spanischpalstek_2d_1.png'),
    image2d: require('./images/draw_spanischpalstek_2d_1.png'),
    image360: require('./images/draw_spanischpalstek_360_1.png')
  },
  {
    knotennummer: 110,
    imagePreview: require('./listimage/draw_centauriknoten_2d_1.png'),
    image2d: require('./images/draw_centauriknoten_2d_1.png'),
    image360: require('./images/draw_centauriknoten_360_1.png')
  },
  {
    knotennummer: 111,
    imagePreview: require('./listimage/draw_nonslipknoten_2d_1.png'),
    image2d: require('./images/draw_nonslipknoten_2d_1.png'),
    image360: require('./images/draw_nonslipknoten_360_1.png')
  },
  {
    knotennummer: 112,
    imagePreview: require('./listimage/draw_kosakenknoten_2d_1.png'),
    image2d: require('./images/draw_kosakenknoten_2d_1.png'),
    image360: require('./images/draw_kosakenknoten_360_1.png')
  },
  {
    knotennummer: 113,
    imagePreview: require('./listimage/draw_kalmueckenknoten_2d_1.png'),
    image2d: require('./images/draw_kalmueckenknoten_2d_1.png'),
    image360: require('./images/draw_kalmueckenknoten_360_1.png')
  },
  {
    knotennummer: 114,
    imagePreview: require('./listimage/draw_doppelterdavy_2d_1.png'),
    image2d: require('./images/draw_doppelterdavy_2d_1.png'),
    image360: require('./images/draw_doppelterdavy_360_1.png')
  },
  {
    knotennummer: 115,
    imagePreview: require('./listimage/draw_diamantknoten2seile_2d_1.png'),
    image2d: require('./images/draw_diamantknoten2seile_2d_1.png'),
    image360: require('./images/draw_diamantknoten2seile_360_1.png')
  },
  {
    knotennummer: 116,
    imagePreview: require('./listimage/draw_crawfort_2d_1.png'),
    image2d: require('./images/draw_crawfort_2d_1.png'),
    image360: require('./images/draw_crawfort_360_1.png')
  },
  {
    knotennummer: 117,
    imagePreview: require('./listimage/draw_pitzenknoten_2d_1.png'),
    image2d: require('./images/draw_pitzenknoten_2d_1.png'),
    image360: require('./images/draw_pitzenknoten_360_1.png')
  },
  {
    knotennummer: 118,
    imagePreview: require('./listimage/draw_lindemann_2d_1.png'),
    image2d: require('./images/draw_lindemann_2d_1.png'),
    image360: require('./images/draw_lindemann_360_1.png')
  },
  {
    knotennummer: 119,
    imagePreview: require('./listimage/draw_turle_2d_1.png'),
    image2d: require('./images/draw_turle_2d_1.png'),
    image360: require('./images/draw_turle_360_1.png')
  },
  {
    knotennummer: 120,
    imagePreview: require('./listimage/draw_orvis_2d_1.png'),
    image2d: require('./images/draw_orvis_2d_1.png'),
    image360: require('./images/draw_orvis_360_1.png')
  },
  {
    knotennummer: 121,
    imagePreview: require('./listimage/draw_tarbuck_2d_1.png'),
    image2d: require('./images/draw_tarbuck_2d_1.png'),
    image360: require('./images/draw_tarbuck_360_1.png')
  },
  {
    knotennummer: 122,
    imagePreview: require('./listimage/draw_klampe_2d_1.png'),
    image2d: require('./images/draw_klampe_2d_1.png'),
    image360: require('./images/draw_klampe_360_1.png')
  },
  {
    knotennummer: 123,
    imagePreview: require('./listimage/draw_stecktauknoten_2d_1.png'),
    image2d: require('./images/draw_stecktauknoten_2d_1.png'),
    image360: require('./images/draw_stecktauknoten_360_1.png')
  },
  {
    knotennummer: 124,
    imagePreview: require('./listimage/draw_warenhaus_2d_1.png'),
    image2d: require('./images/draw_warenhaus_2d_1.png'),
    image360: require('./images/draw_warenhaus_360_1.png')
  },
  {
    knotennummer: 125,
    imagePreview: require('./listimage/draw_chirugenknoten_2d_1.png'),
    image2d: require('./images/draw_chirugenknoten_2d_1.png'),
    image360: require('./images/draw_chirugenknoten_360_1.png')
  },
  {
    knotennummer: 126,
    imagePreview: require('./listimage/draw_backhandhitch_2d_1.png'),
    image2d: require('./images/draw_backhandhitch_2d_1.png'),
    image360: require('./images/draw_backhandhitch_360_1.png')
  },
  {
    knotennummer: 127,
    imagePreview: require('./listimage/draw_backhandhitch_2d_1.png'),
    image2d: require('./images/draw_backhandhitch_2d_1.png'),
    image360: require('./images/draw_backhandhitch_360_1.png')
  },
  {
    knotennummer: 128,
    imagePreview: require('./listimage/draw_gordingstek_2d_1.png'),
    image2d: require('./images/draw_gordingstek_2d_1.png'),
    image360: require('./images/draw_gordingstek_360_1.png')
  },
  {
    knotennummer: 129,
    imagePreview: require('./listimage/draw_mittschiffmann_2d_1.png'),
    image2d: require('./images/draw_mittschiffmann_2d_1.png'),
    image360: require('./images/draw_mittschiffmann_360_1.png')
  },
  {
    knotennummer: 130,
    imagePreview: require('./listimage/draw_feuerwehrbunsch_2d_1.png'),
    image2d: require('./images/draw_feuerwehrbunsch_2d_1.png'),
    image360: require('./images/draw_feuerwehrbunsch_360_1.png')
  },
];
